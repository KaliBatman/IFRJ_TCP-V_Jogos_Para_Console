﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour 
{
    [SerializeField]
    protected StageManager stageManager;

    [SerializeField]
    protected int facingDirection;

    [SerializeField]
    protected float jumpForce, life, raycastCheckDistance, xScale;

    [SerializeField]
    protected LayerMask raycastLayerMask;

    [SerializeField]
    protected Vector2 speed, raycastOffset;

    protected Rigidbody2D rb;
    protected SpriteRenderer spriteRenderer;
    protected Animator animator;

    public virtual Rigidbody2D getRb
    {
        get { return rb; }
    }

    public virtual int setFacingDirection
    {
        get { return facingDirection; }

        set
        { 
            facingDirection = value;

            if (value < 0) { transform.localScale = new Vector3 (xScale * -1, transform.localScale.y, transform.localScale.z); }
            else { transform.localScale = new Vector3(xScale, transform.localScale.y, transform.localScale.z); } 
        }
    }

    public virtual float setLife(float value = 0)
    {
        this.life += value;

        if (life <= 0)
        {
            Kill();
        }

        return life;
    }

    public virtual void Kill()
    {
        Destroy(this.gameObject);
    }

	protected virtual void Start () 
    {
        rb = this.GetComponent<Rigidbody2D>();
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        animator = this.GetComponent<Animator>();
        xScale = this.transform.localScale.x;
	}

    protected virtual bool RaycastCheck(Vector2 direction)
    {
        Vector2 position = new Vector2(transform.position.x - raycastOffset.x * facingDirection, transform.position.y + raycastOffset.y);

        RaycastHit2D hit = Physics2D.Raycast(position, direction, raycastCheckDistance, raycastLayerMask);
        Debug.DrawRay(position, direction, Color.black, 3f);

        if (hit.collider != null)
        {
            return true;
        }

        return false;
    }

    public virtual void setStageManager(StageManager stageManager)
    {
        this.stageManager = stageManager; 
    }
}
