﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character 
{
    [SerializeField]
    protected Gun activeGun;

    bool dead;

    [SerializeField]
    protected float viewAngle, chaseRange;

    public enum States { Patrolling, Alerted };

    States state;

    protected EnemySpawner spawner;
    GameObject target;

    public States getState
    {
        get { return state; }
    }

   protected override void Start()
    {
        base.Start();

        state = States.Patrolling;
    }

   protected virtual void FixedUpdate()
   {
       if (dead) { return; }

       Routine();

       if (rb.velocity != Vector2.zero) { animator.SetBool("Run", true); }
       else { animator.SetBool("Run", false); }
   }

   public override float setLife(float value = 0)
   {
       this.life += value;

       if (life <= 0)
       {
           Forget();
           dead = true;
           animator.SetBool("Die", true);
           Invoke("Kill", 1);
       }

       return life;
   }

   protected virtual void Routine()
   {
       if (state == States.Patrolling)
       {
           this.rb.velocity = new Vector2(facingDirection * speed.x, 0);

           if (RaycastCheck(Vector2.right * facingDirection))
           {
               setFacingDirection = -facingDirection;
           }
       }
       else if (state == States.Alerted)
       {
           if (target.transform.position.x > this.transform.position.x)
           {
               setFacingDirection = 1;
           }
           else if (target.transform.position.x < this.transform.position.x)
           {
               setFacingDirection = -1;
           }
           animator.SetBool("Gun", true);
           activeGun.Shoot();

           if (Vector2.Distance(this.transform.position, target.transform.position) > chaseRange) { rb.velocity = new Vector2(speed.x * facingDirection, rb.velocity.y); } 
           else { rb.velocity = new Vector2(0, rb.velocity.y); }

           if (RaycastCheck(Vector2.right * facingDirection))
           {
               rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
           }
       }
   }

   protected override bool RaycastCheck(Vector2 direction)
   {
       Vector2 position = new Vector2(transform.position.x - raycastOffset.x * facingDirection, transform.position.y + raycastOffset.y);

       RaycastHit2D hit = Physics2D.Raycast(position, direction, raycastCheckDistance, raycastLayerMask);
       Debug.DrawRay(position, direction, Color.black, 3f);

       if (hit.collider != null && !hit.collider.gameObject.CompareTag("Player"))
       {
           return true;
       }

       return false;
   }

   public override void Kill()
   {
       stageManager.setAlarm = false;
       stageManager.addEnemy(true, this);
       base.Kill();
   }

   public void Watch(Collider2D c)
   {
       if (dead) { return; }
       if (c.gameObject.CompareTag("Player"))
       {
           Vector2 direction = c.transform.position - transform.position;
           float angle = Vector2.Angle(direction, transform.right * setFacingDirection);

           if (angle < viewAngle * 0.5f)
           {
               RaycastHit2D hit = Physics2D.Raycast(transform.position, direction.normalized, Vector2.Distance(c.transform.position, transform.position), raycastLayerMask);
               Debug.DrawRay(transform.position, direction, Color.red, 3f);

               if (hit.collider != null && hit.collider.gameObject.CompareTag("Player"))
               {
                   if (!stageManager.setAlarm)
                   {
                       stageManager.setAlarm = true;
                   }

                   state = States.Alerted;
                   rb.velocity = Vector2.zero;
                   target = c.gameObject;
               }
           }
       }
   }

   public void Forget()
   {
       stageManager.setAlarm = false;
       rb.velocity = Vector2.zero;
       state = States.Patrolling;
       animator.SetBool("Gun", false);
   }
}
