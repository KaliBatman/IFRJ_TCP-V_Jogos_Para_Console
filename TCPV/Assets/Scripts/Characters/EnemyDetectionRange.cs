﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetectionRange : MonoBehaviour 
{
    Enemy enemy;

	// Use this for initialization
	void Start () 
    {
        enemy = transform.parent.gameObject.GetComponent<Enemy>();
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        enemy.Watch(c);
    }

    void OnTriggerStay2D(Collider2D c)
    {
        enemy.Watch(c);
    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.gameObject.CompareTag("Player") && enemy.getState == Enemy.States.Patrolling)
        {
            enemy.Forget();
        }
    }
}
