﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : Character
{
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] Transform[] spawnPoint;
    [SerializeField] float timeDestroy = 1.0f;

    float currentCooldown;
    [SerializeField] float cooldown = 1;

    public float force = 100;

    void Update ()
    {
        Cooldown();
        if (this.currentCooldown == 0)
        {
            Shoot();
            this.currentCooldown = cooldown;
        }
	}

    void Shoot()
    {
        for (int i = 0; i < spawnPoint.Length; i++)
        {
            var bullet = (GameObject)Instantiate(
            bulletPrefab,
            spawnPoint[i].position,
            spawnPoint[i].rotation);

            bullet.GetComponent<Rigidbody2D>().AddForce((spawnPoint[i].transform.right * -1) * force, ForceMode2D.Impulse);
            Destroy(bullet, timeDestroy);
        }
    }

    void Cooldown()
    {
        if (this.currentCooldown > 0)
            this.currentCooldown -= Time.deltaTime;
        else
            this.currentCooldown = 0;
    }
}
