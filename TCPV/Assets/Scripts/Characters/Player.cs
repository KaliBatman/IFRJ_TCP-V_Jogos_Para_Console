﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    [SerializeField]
    int playerIndex;

    [SerializeField]
    float dashSpeed, dashInterval;

    [SerializeField]
    Gun activeGun;

    float originalGravity;

    bool dashing, canDash;

    public bool dead;

    protected override void Start()
    {
        base.Start();
        originalGravity = rb.gravityScale;
    }

    public override float setLife(float value = 0)
    {
        this.life += value;

        if (life <= 0)
        {
            dead = true;
            animator.SetBool("Run", false);
            animator.SetBool("Jump", false);
            animator.SetBool("Crouch", false);
            animator.SetBool("Dash", false);
            animator.SetBool("Die", true);
            rb.velocity = Vector2.zero;
            stageManager.CheckDead(false);
            Invoke("GameOver", 10);
        }

        return life;
    }

    void GameOver()
    {
        stageManager.CheckDead(true);
    }

    void Movement()
    {
        if (Input.GetButtonDown("Dash_P" + playerIndex) && canDash)
        {
            if (playerIndex == 1 && animator.GetBool("Crouch")) 
            {
                dashing = true;
                animator.SetBool("Dash", dashing);
                canDash = false;
                rb.velocity = Vector2.zero;
                rb.AddForce((Vector2.right * facingDirection) * dashSpeed, ForceMode2D.Impulse);
                rb.gravityScale = 0;
                Invoke("ResetDashing", dashInterval);
            }
            else if (playerIndex == 2 && animator.GetBool("Jump")) 
            {
                dashing = true;
                animator.SetBool("Dash", dashing);
                canDash = false;
                rb.velocity = Vector2.zero;
                rb.AddForce((Vector2.right * facingDirection) * dashSpeed, ForceMode2D.Impulse);
                rb.gravityScale = 0;
                Invoke("ResetDashing", dashInterval);
            }
        }

        if (dashing || animator.GetBool("Crouch")) { return; }

        #region gerenciar direção do personagem
        if (Input.GetAxis("Horizontal_P" + playerIndex) > 0) { setFacingDirection = 1; }
        else if (Input.GetAxis("Horizontal_P" + playerIndex) < 0) { setFacingDirection = -1; }
        #endregion

        this.rb.velocity = new Vector2((Input.GetAxisRaw("Horizontal_P" + playerIndex) * speed.x), this.rb.velocity.y);

    }

    void ResetDashing()
    {
        dashing = false;
        animator.SetBool("Dash", dashing);
        rb.gravityScale = originalGravity;
        this.rb.velocity = Vector2.zero;
    }

    bool IsGrounded()
    {
        for (int i = -1; i < 2; i++)
        {
            Vector2 position = new Vector2(transform.position.x - (raycastOffset.x * i) * facingDirection, transform.position.y + raycastOffset.y);
            Vector2 direction = Vector2.down;

            RaycastHit2D hit = Physics2D.Raycast(position, direction, raycastCheckDistance, raycastLayerMask);
            Debug.DrawRay(position, direction,Color.black, 3f);

            if (hit.collider != null)
            {
                canDash = true;
                rb.gravityScale = originalGravity;
                return true;
            }
        }
        return false;
    }

    protected override bool RaycastCheck(Vector2 direction)
    {
        for (int i = -1; i < 2; i++)
        {
            Vector2 position = new Vector2(transform.position.x - (raycastOffset.x * i) * facingDirection, transform.position.y + raycastOffset.y);

            RaycastHit2D hit = Physics2D.Raycast(position, direction, raycastCheckDistance, raycastLayerMask);
            Debug.DrawRay(position, direction, Color.black, 3f);

            if (hit.collider != null)
            {
                return true;
            }
        }

        return false;
    }

    void FixedUpdate()
    {
        if (dead) { return; }
        Movement();
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (dashing) { rb.gravityScale = originalGravity; }

        if (IsGrounded())
        {
           animator.SetBool("Jump", false);
        }

        if (dead && c.gameObject.CompareTag("Player"))
        {
            dead = false;
            CancelInvoke("GameOver");
            animator.SetBool("Die", false);
        }
    }

    void OnTriggerStay2D(Collider2D c)
    {
        if (c.gameObject.CompareTag("Finish"))
        {
            stageManager.FinishStage(true, this.playerIndex);
        }
    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.gameObject.CompareTag("Finish"))
        {
            stageManager.FinishStage(false, this.playerIndex);
        }
    }

    void OnCollisionExit2D(Collision2D c)
    {
        if (!IsGrounded())
        {
            rb.gravityScale = originalGravity;
            animator.SetBool("Jump", true);
        }
    }

    void ZeroInCrouch()
    {
        this.rb.velocity = Vector2.zero;
    }

    void Update()
    {
        if (dead) { return; }

        if (!canDash)
        {
            IsGrounded();
        }

        if (Input.GetAxis("Horizontal_P" + playerIndex) != 0 && rb.velocity.x != 0) { animator.SetBool("Run", true); }
        else { animator.SetBool("Run", false); }

        if (Input.GetButton("Fire_P" + playerIndex))
        {
            animator.SetBool("Shooting", true);
            activeGun.Shoot();
        }
        else if (!Input.GetButton("Fire_P" + playerIndex))
        {
            animator.SetBool("Shooting", false);
        }

        if (Input.GetAxis("Vertical_P" + playerIndex) > 0)
        {
            if (!IsGrounded()) { return; }
            animator.SetBool("Crouch", true);
        }

        else if (!Input.GetButton("Vertical_P" + playerIndex)) 
        { 
            if (RaycastCheck(Vector2.up)) { return; }
            animator.SetBool("Crouch", false); 
        }

        if (Input.GetButtonDown("Jump_P" + playerIndex) && IsGrounded() && !animator.GetBool("Crouch"))
        {
            rb.gravityScale = originalGravity;
            this.animator.SetBool("Jump", true);
            this.rb.AddForce(Vector2.up * (jumpForce), ForceMode2D.Impulse);
        }
    }
}
