﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour 
{
    [SerializeField]
    Camera camera1, camera2;

    [SerializeField]
    Player player1, player2;

    [SerializeField]
    float speed, maxDistance;

    Animator animator1, animator2;

    Rigidbody2D rb1, rb2;

    BoxCollider2D collider;

    [SerializeField]
    Vector2 ogOffset, newOffset, ogSize, newSize;

    void Start()
    {
        rb1 = camera1.gameObject.GetComponent<Rigidbody2D>();
        rb2 = camera2.gameObject.GetComponent<Rigidbody2D>();
        animator1 = camera1.gameObject.GetComponent<Animator>();
        animator2 = camera2.gameObject.GetComponent<Animator>();
        collider = camera1.gameObject.GetComponent<BoxCollider2D>();
    }

    public void AlarmRing(bool ring)
    {
        animator1.SetBool("Alarm", ring);
        animator2.SetBool("Alarm", ring);
    }

    void LateUpdate()
    {
        rb1.MovePosition(Vector2.MoveTowards(camera1.transform.position, new Vector2(player1.transform.position.x, 8), 0.01f * speed));
        rb2.MovePosition(Vector2.MoveTowards(camera2.transform.position, new Vector2(player2.transform.position.x, 8), 0.01f * speed));

        if (Vector2.Distance(player1.transform.position, player2.transform.position) < maxDistance)
        {
            camera1.rect = new Rect(0, 0, 1, 1);
            camera1.orthographicSize = 16;
            collider.offset = ogOffset;
            collider.size = ogSize;
        }
        else { camera1.rect = new Rect(0, 0.5f, 1, 0.5f); collider.offset = newOffset; collider.size = newSize; camera1.orthographicSize = 8; }
    }
}
