﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSwitch : MonoBehaviour 
{
    [SerializeField]
    Transform door, openedPos;

    [SerializeField]
    float openSpeed;

    bool active;

    void Open()
    {
        door.position = Vector2.MoveTowards(door.position, openedPos.position, (Time.deltaTime * 60) * openSpeed);
        if (door.position.y >= openedPos.localPosition.y)
        {
            CancelInvoke("Open"); return;
        }
        else
        {
            Invoke("Open", 0.01f);
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.CompareTag("Player") && !active)
        {
            active = true;
            this.transform.Rotate(0, 0, 45);
            Open();
        }
    }
}
