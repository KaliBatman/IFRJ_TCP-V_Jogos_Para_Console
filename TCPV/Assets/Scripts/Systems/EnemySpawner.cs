﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    StageManager stageManager;

    [SerializeField]
    GameObject enemyPrefab, enemiesGroup;

    [SerializeField]
    float interval;

    [SerializeField]
    int cycles;

    public void InstantiateEnemy(bool repeating)
    {
        if (cycles > 0)
        {
            GameObject newEnemy = (GameObject)Instantiate(enemyPrefab, this.transform.position, Quaternion.identity);
            newEnemy.GetComponent<Enemy>().setStageManager(this.stageManager);
            //newEnemy.transform.SetParent(enemiesGroup.transform);
            stageManager.addEnemy(false, newEnemy.GetComponent<Enemy>());

            if (repeating)
            {
                Invoke("InstantiateEnemy", interval);
            }
            cycles--;
        }
    }

    public void StopInstantiate()
    {
        CancelInvoke("InstantiateEnemy");
    }
}
