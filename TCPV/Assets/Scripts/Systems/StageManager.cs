﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageManager : MonoBehaviour 
{
    [SerializeField]
    EnemySpawner[] spawners;

    [SerializeField]
    Player[] players;

    [SerializeField]
    CameraManager cameraManager;

    [SerializeField]
    List<Enemy> enemies;

    [SerializeField]
    int nextStage, actualStage;

    bool alarm, player1Finished, player2Finished;

    public void FinishStage(bool finished, int playerId)
    {
        if (playerId == 1) { player1Finished = finished; }
        else if (playerId == 2) { player2Finished = finished; }
        if (finished) { print(1); }

        if (player1Finished && player2Finished)
        {
            SceneManager.LoadScene(nextStage);
        }
    }

    public void CheckDead(bool die)
    {
        if (players[0].dead && players[1].dead)
        {
            SceneManager.LoadScene(actualStage);
        }

        if (die) { SceneManager.LoadScene(actualStage); }
    }

    public bool setAlarm
    {
        get { return this.alarm; }
        set 
        {
            alarm = value; 
            cameraManager.AlarmRing(value);

            if (value == true)
            {
                foreach (EnemySpawner spawner in spawners)
                {
                    spawner.InstantiateEnemy(true);
                }
            }
            else 
            {
                foreach (EnemySpawner spawner in spawners)
                {
                    spawner.StopInstantiate();
                }
            }
        }
    }

    public CameraManager getCamManager
    {
        get { return cameraManager; }
    }

    public void addEnemy(bool remove, Enemy enemy)
    {
        if (remove)
        {
            enemies.Remove(enemy);
        }
        else
        {
            enemies.Add(enemy);
        }
    }
}
