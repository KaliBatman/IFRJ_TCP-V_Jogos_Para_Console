﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour 
{
    protected Character character;

    [SerializeField]
    protected GameObject shotPrefab;

    [SerializeField]
    protected Vector2 shotSpeed;

    [SerializeField]
    Transform shotSpawn; 

    [SerializeField]
    protected float shotDamage, shotCd;

    protected bool canShoot;

	protected virtual void Start () 
    {
        character = this.gameObject.GetComponent<Character>();
        canShoot = true;
	}

    public virtual void Shoot()
    {
        if (!canShoot) { return; }

        canShoot = false;
        GameObject newShot = GameObject.Instantiate(shotPrefab, shotSpawn.position, Quaternion.identity);
        newShot.GetComponent<Shot>().setOwner = character;
        newShot.tag = this.gameObject.tag + "Shot";
        newShot.GetComponent<Shot>().setSpeed = new Vector2(shotSpeed.x * character.setFacingDirection, shotSpeed.y);
        newShot.GetComponent<Shot>().setDamage = shotDamage;
        Invoke("ValidateShot", shotCd);
    }

    protected virtual void ValidateShot()
    {
        canShoot = true;
    }
}
