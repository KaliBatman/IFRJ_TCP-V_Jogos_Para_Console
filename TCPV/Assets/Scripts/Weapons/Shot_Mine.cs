﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot_Mine : Character
{
    Character owner;

    float damage;

    protected override void Start()
    {
        //base.Start();
        //rb.velocity = speed;
        //setFacingDirection = owner.setFacingDirection;
    }

    public Character setOwner
    {
        set { owner = value; }
    }

    public Vector2 setSpeed
    {
        set { speed = value; }
    }

    public float setDamage
    {
        set { damage = value; }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.GetComponent<Character>() && c.gameObject != owner.gameObject && !owner.gameObject.CompareTag(c.gameObject.tag))
        {
            c.gameObject.GetComponent<Character>().setLife(damage);
            Destroy(this.gameObject);
        }
    }
}
